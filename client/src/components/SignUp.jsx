import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Link, Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import CircularProgress from "@mui/material/CircularProgress";
import Loading from "./Loading";
function SignUp() {
  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [isSuperAdmin, setIsSuperAdmin]=useState(  localStorage.getItem("currentUser") !== "undefined" &&
  localStorage.getItem("currentUser") !== null && localStorage.getItem("userRole")==="superAdmin");
 
  const [user, setUser] = useState({
    
    email: "",
    password: "",
    password_c: "",
    role:""
  });
  const [isError, setIsError] = useState(false);

  function handleChange(event) {
    const { name, value } = event.target;
    
    setUser({ ...user, [name]: value });
   
  }


  async function handleRegister(e) {
    e.preventDefault();

    setUser(user.role=isSuperAdmin ? "admin" : "buyer");

    if (user.password !== user.password_c) {
      setIsError(true);
      const { name, value } = e.target;
    
      setUser({ ...user, [name]: value });
    } else {
      setLoading(true);
      axios
        .post("http://localhost:3000/api/signup", user)
        .then(function (response) {
          console.log(response.data.id);
          setTimeout(() => {
            setLoading(false);
            isSuperAdmin ? navigate("/dashboard") : navigate("/login");
            
          }, 2000);
        })
        .catch(function (error) {
          console.log(error.response);
        });
    }
    
  }

  return (
    <div>
      {loading && <Loading />}

      <h1 className="heading">Create A New Account</h1>
      <form className="formCard" border="dark" onSubmit={handleRegister}>
      <Form.Text className="mb-3 errors" style={{ display: isError ? "block" : "none" }}>
      Passwords don't match. Please try again.
      </Form.Text>
      
        <Form.Group className="mb-3" controlId="formBasicEmail">
        <input name="role" defaultValue={ isSuperAdmin ? "admin" : "buyer"} hidden /> 
          <Form.Label>Email address</Form.Label>
          <Form.Control
            name="email"
            type="email"
            placeholder="Enter email"
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            name="password"
            type="password"
            placeholder="Password"
            onChange={handleChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Confirm Password</Form.Label>
          <Form.Control
            name="password_c"
            type="password"
            placeholder="Password"
            onChange={handleChange}
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
}

export default SignUp;
