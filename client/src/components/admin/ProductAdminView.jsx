import React, { useState, useEffect } from "react";
import TextTruncate from "react-text-truncate";
import { Link } from "react-router-dom";
import { Card, Col, Row, Button, Accordion, Form } from "react-bootstrap";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Loading from "../Loading";
import EditIcon from "@mui/icons-material/Edit";
import { bounce, fadeInUp } from "react-animations";
import styled, { keyframes } from "styled-components";
import axios from "axios";

function ProductAdminView(props) {
  const [isEditable, setIsEditable] = useState(false);
  const fadeAnimation = keyframes`${fadeInUp}`;
  const [editProduct, setEditProduct] = useState({});
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");

  //animation
  const FadeDiv = styled.div`
    animation: 1s ${fadeAnimation};
  `;

  function handleChange(event) {
    const { name, value } = event.target;

    setEditProduct({ ...editProduct, [name]: value });
  }
  console.log(props.id);
  async function handleEdit(e) {
    e.preventDefault();
   
    const data = new FormData();

    data.append("file", image);
    data.append("upload_preset", "quickshop");
    data.append("cloud_name", "gapstars");
    // data.append("quality", "q_auto");

    
    fetch("https://api.cloudinary.com/v1_1/gapstars/image/upload/", {
      method: "post",
      body: data,
    })
      .then((resp) => resp.json())
      .then((data) => {
        setTimeout(() => {
          //console.log(data.url);
          setUrl(data.url);
          console.log(data.url);
          setEditProduct((editProduct.imageURL = data.url));
          console.log(editProduct);
          setLoading(true);
          axios
            .put(
              "http://localhost:3000/api/products/edit/" + props.id,
              editProduct
            )
            .then(function (response) {
              console.log(response);
              setLoading(false);
              window.location.reload();
            })
            .catch(function (error) {
              console.log(error.response);
            });
        }, 2000);
      })
      .catch((err) => console.log(err));
  }

  async function handleDelete() {
    setLoading(true);
    axios
      .delete("http://localhost:3000/api/products/delete/" + props.id)
      .then(function (response) {
        console.log(response);
        setLoading(false);
        window.location.reload();
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }

  return (
    <div>
      {loading && <Loading />}
      <div className="dashboard">
        <Card className="productAdmin">
          <Row>
            <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
              <img
                className="adminProdImg"
                src={props.imageURL}
                style={{ width: "50px" }}
              />
            </Col>
            <Col className="d-flex flex-column min-vh-10 justify-content-center ">
              <h2>{props.name}</h2>
              <p>{props.description}</p>
              <p> ${props.price} </p>
              <p>{props.quantity} items left</p>
            </Col>
            <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
              <Row>
                <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                  <EditIcon
                    fontSize="large"
                    onClick={() =>
                      setIsEditable((prevValue) => {
                        return !prevValue;
                      })
                    }
                  />
                </Col>

                <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                  <DeleteOutlineIcon className="deleteIcon" fontSize="large" onClick={handleDelete} />
                </Col>
              </Row>
            </Col>
          </Row>

          {isEditable && (
            <Form className="formCard" onSubmit={handleEdit}>
              <Form.Group controlId="formFile" className="mb-3">
                <Form.Label>Change product image</Form.Label>
                <Form.Control
                  type="file"
                  accept="image/*"
                  name="image"
                  onChange={(e) => setImage(e.target.files[0])}
                />
              </Form.Group>

              <Form.Group controlId="formFile" className="mb-3">
                <Form.Label>Product Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  defaultValue={props.name}
                  onChange={handleChange}
                />
              </Form.Group>

              <Row>
                <Col>
                  <Form.Group controlId="formFile" className="mb-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control
                      type="number"
                      name="price"
                      defaultValue={props.price}
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="formFile" className="mb-3">
                    <Form.Label>Stock Remaining</Form.Label>
                    <Form.Control
                      type="number"
                      name="quantity"
                      defaultValue={props.quantity}
                      onChange={handleChange}
                    />
                  </Form.Group>
                </Col>
              </Row>

              <Form.Group controlId="formFile" className="mb-3">
                <Form.Label>Product Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="description"
                  defaultValue={props.description}
                  onChange={handleChange}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          )}
        </Card>
      </div>
    </div>
  );
}

export default ProductAdminView;
