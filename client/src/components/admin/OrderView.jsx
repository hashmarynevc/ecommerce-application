import React, { useState, useEffect } from "react";
import TextTruncate from "react-text-truncate";
import { Link } from "react-router-dom";
import { Card, Col, Row, Button, Accordion, Form } from "react-bootstrap";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Loading from "../Loading";
import EditIcon from "@mui/icons-material/Edit";
import { bounce, fadeInUp } from "react-animations";
import styled, { keyframes } from "styled-components";
import axios from "axios";

function OrderView(props) {

  const [orderStatus, setOrderStatus] = useState();
  const [editStatus, setEditStatus] = useState({});
  var status=props.status;
 
  async function handleChange(e) {
   status=e.target.value;
   console.log(status);
    // setOrderStatus(e.target.value);
    // console.log(orderStatus);
  }
  async function handleClick() {

    setEditStatus(editStatus.orderStatus=status);
    axios
      .put("http://localhost:3000/api/order/edit/" + props.id, editStatus)
      .then(function (response) {
        console.log(response);
        window.location.reload();
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }

 

  return (
    <div>
      <div className="dashboard">
        <Card className="productAdmin">
          <Row>
            <Col className="d-flex flex-column min-vh-10 justify-content-center ">
              <h6>Order ID: {props.id} </h6>
              
              <table>
                <tr>
                  <td>
                    <strong>Product ID</strong>
                  </td>
                  <td>
                    <strong>Quantity</strong>
                  </td>
                </tr>

                {props.products.map((product, index) => {
                  return (
                    <tr>
                      <td>{product.product}</td>
                      <td>{product.quantity}</td>
                    </tr>
                  );
                })}
              </table>

              <h6>Shipping Address: {props.address} </h6>
              <h6>Total: ${props.price} </h6>
              <h6>Payment Method: {props.payment} </h6>
            </Col>
            <Col className="d-flex flex-column min-vh-10 justify-content-center ">
              
                <Row>
                  <Col>
                    <Form.Select aria-label="Default select example" onChange={handleChange} name="status">
                    <option value={props.satus}>{props.status}</option>
                      <option value="Confirmed">Confirmed</option>
                      <option value="Dispatched">Dispatched</option>
                      <option value="Delivered">Delivered</option>
                    </Form.Select>
                  </Col>
                  <Col>
                    <Button variant="outline-primary" onClick={handleClick}>
                      Save
                    </Button>
                  </Col>
                </Row>
              
            </Col>
          </Row>
        </Card>
      </div>
    </div>
  );
}

export default OrderView;
