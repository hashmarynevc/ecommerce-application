import React, { useState, useEffect } from "react";
import ProductAdminView from "./ProductAdminView";
import axios from "axios";
import { Card, Col, Row, Button, Accordion, Form } from "react-bootstrap";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Loading from "../Loading";
import EditIcon from "@mui/icons-material/Edit";
import { bounce, fadeInUp } from "react-animations";
import styled, { keyframes } from "styled-components";

function Dashboard() {
  const [loading, setLoading] = useState(false);
  const [products, setProducts] = useState([]);
  const [image, setImage] = useState("");
  const [url, setUrl] = useState("");
  const [isEditable, setIsEditable] = useState(false);

  const [newProduct, setNewProduct] = useState({
    name: "",
    price: "",
    description: "",
    quantity: "",
    imageURL: "",
  });

  function handleChange(event) {
    const { name, value } = event.target;

    setNewProduct({ ...newProduct, [name]: value });
  }

  async function handleSubmit(e) {
    e.preventDefault();
    const data = new FormData();

    data.append("file", image);
    data.append("upload_preset", "quickshop");
    data.append("cloud_name", "gapstars");
    // data.append("quality", "q_auto");

    setLoading(true);
    fetch("https://api.cloudinary.com/v1_1/gapstars/image/upload/", {
      method: "post",
      body: data,
    })
      .then((resp) => resp.json())
      .then((data) => {
        setTimeout(() => {
          setNewProduct((newProduct.imageURL = data.url));

          axios
            .post("http://localhost:3000/api/products/add", newProduct)
            .then(function (response) {
              setLoading(false);
              window.location.reload();
            })
            .catch(function (error) {
              console.log(error.response);
            });
        }, 2000);
      })
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    axios
      .get("http://localhost:3000/api/products")
      .then((res) => setProducts(res.data))
      //.then((res) => console.log(res.data[0]._id))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div>
      {loading ? (
        <Loading />
      ) : (
        <>
          <h1 className="heading">Products</h1>

          <Card style={{ width: "80%" }} className="adminPage">
            <Card style={{ width: "95%" }} className="adminPage">
              <Accordion defaultActiveKey="0">
                <Accordion.Item eventKey="1">
                  <Accordion.Header
                    variant="outline-dark"
                    style={{ backgroundColor: "transparent!important" }}
                  >
                    <Button
                      variant="outline-dark"
                      style={{
                        height: "80px",
                        width: "100%",
                        fontSize: "20px",
                      }}
                    >
                      + Add Product
                    </Button>
                  </Accordion.Header>
                  <Accordion.Body>
                    <Form className="formCard" onSubmit={handleSubmit}>
                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Upload product Image</Form.Label>
                        <Form.Control
                          type="file"
                          accept="image/*"
                          name="image"
                          onChange={(e) => setImage(e.target.files[0])}
                        />
                      </Form.Group>

                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Product Name</Form.Label>
                        <Form.Control
                          type="text"
                          name="name"
                          onChange={handleChange}
                        />
                      </Form.Group>

                      <Row>
                        <Col>
                          <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                              type="number"
                              name="price"
                              onChange={handleChange}
                            />
                          </Form.Group>
                        </Col>
                        <Col>
                          <Form.Group controlId="formFile" className="mb-3">
                            <Form.Label>Stock Remaining</Form.Label>
                            <Form.Control
                              type="number"
                              name="quantity"
                              onChange={handleChange}
                            />
                          </Form.Group>
                        </Col>
                      </Row>

                      <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label>Product Description</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          name="description"
                          onChange={handleChange}
                        />
                      </Form.Group>
                      <Button variant="primary" type="submit">
                        Submit
                      </Button>
                    </Form>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
            </Card>
            {products.map((product, index) => {
              return (
                <div>
                  <ProductAdminView
                    key={index}
                    id={product._id}
                    name={product.name}
                    description={product.description}
                    price={product.price}
                    quantity={product.quantity}
                    imageURL={product.imageURL}
                  />
                </div>
              );
            })}
          </Card>
        </>
      )}
    </div>
  );
}

export default Dashboard;
