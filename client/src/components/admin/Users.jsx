import React, { useState, useEffect } from "react";
import TextTruncate from "react-text-truncate";
import { Link } from "react-router-dom";
import { Container, Card, Button, CardGroup, Col, Form } from "react-bootstrap";
import axios from "axios";
import UserView from './UserView'

function Users() {

    const [users, setUsers] = useState([]);
 
  useEffect(() => {
    axios
      .get("http://localhost:3000/api/users/")
      .then((res) => setUsers(res.data))
      //.then((res) => console.log(res.data[0]._id))
      .catch((err) => console.error(err));
  }, [])
  
  return (
    <div>
<Card style={{ width: "80%" }} className="adminPage">
        
        {users.map((user, index) => {
          return (
            <div>
            <UserView
            key={index}
            id={user._id}
            email={user.email}
            role={user.role}
           
          />
            </div>
          );
        })}
      </Card>
    </div>
  );
}

export default Users;
