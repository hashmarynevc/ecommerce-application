import React, { useState, useEffect } from "react";
import TextTruncate from "react-text-truncate";
import { Link } from "react-router-dom";
import { Card, Col, Row, Button, Accordion, Form } from "react-bootstrap";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import Loading from "../Loading";
import EditIcon from "@mui/icons-material/Edit";
import { bounce, fadeInUp } from "react-animations";
import styled, { keyframes } from "styled-components";
import axios from "axios";

function UserView(props) {
  const [isAdmin, setIsAdmin] = useState(props.role === "admin");
  const [loading, setLoading] = useState(false);
  const [editUser, setEditUser]=useState({});

  async function handleClick() {
  
    setLoading(true);
    isAdmin ? setEditUser(editUser.role="buyer") : setEditUser(editUser.role="admin");
    axios
      .put("http://localhost:3000/api/user/edit/" + props.id, editUser)
      .then(function (response) {
        console.log(response);
        setLoading(false);
        window.location.reload();
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }

  return (
    <div>
      {loading && <Loading />}
      <div className="dashboard">
        <Card className="productAdmin">
          <Row>
            <Col className="d-flex flex-column min-vh-10 justify-content-center ">
              <h2>{props.email}</h2>
            </Col>
            <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
              <Row>
                <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                  
                {isAdmin ? <Button variant="outline-danger" onClick={handleClick}>Remove Admin</Button> : <Button variant="outline-primary" onClick={handleClick}>Make Admin</Button>}
                  
                </Col>
              </Row>
            </Col>
          </Row>
        </Card>
      </div>
    </div>
  );
}

export default UserView;
