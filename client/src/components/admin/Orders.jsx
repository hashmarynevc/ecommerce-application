import React, { useState, useEffect } from "react";
import TextTruncate from "react-text-truncate";
import { Link } from "react-router-dom";
import { Container, Card, Button, CardGroup, Col, Form } from "react-bootstrap";
import axios from "axios";
import OrderView from "./OrderView";

function Order() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:3000/api/orders/")
      .then((res) => setOrders(res.data))
      //.then((res) => console.log(res.data))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div>
      <Card style={{ width: "80%" }} className="adminPage">
        {orders.map((order, index) => {
          return (
          
            <div>
              <OrderView
                key={index}
                id={order._id}
                products={order.products}
                address={order.shippingAddress}
                price={order.totalPrice}
                payment={order.paymentMethod}
                status={order.orderStatus}
              />
            </div>
          );
        })}
      </Card>
    </div>
  );
}

export default Order;
