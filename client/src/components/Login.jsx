import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Link, Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import Loading from "./Loading";
function Login() {
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  // const [profile, setProfile] = useState({user:"", fname:"", lname:"", contact:"", address:"" });
  let navigate = useNavigate();
  async function handleLogin(e) {
    e.preventDefault();
    const form = e.target;
    const user = {
      email: form[0].value,
      password: form[1].value,
    };
    //console.log(user);
    setLoading(true);
    axios
      .post("http://localhost:3000/api/login", user)
      .then(function (response) {
        console.log(response.data.message);
        if (response.data.message === "Invalid Username or Password") {
          setIsError(true);
          setLoading(false);
        }
        //console.log(errors);
        if (response.data.message === "Sucess") {
          localStorage.setItem("currentUser", response.data.user);
          localStorage.setItem("userRole", response.data.role);
          setTimeout(() => {
            setLoading(false);
            response.data.role === "admin" ||
            response.data.role === "superAdmin"
              ? navigate("/dashboard")
              : navigate("/");

            window.location.reload();
          }, 2000);
        }
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }

  return (
    <div>
      {loading ? (
        <Loading />
      ) : (
        <>
          {" "}
          <h1 className="heading">Login</h1>
          <Form className="formCard" border="dark" onSubmit={handleLogin}>
            <Form.Text
              className="mb-3 errors"
              style={{ display: isError ? "block" : "none" }}
            >
              Invalid email or password. Please try again.
            </Form.Text>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                name="email"
                type="email"
                placeholder="Enter email"
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                name="password"
                type="password"
                placeholder="Password"
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formBasicCheckbox">
              No account? Create one <Link to="/signup">here</Link>
            </Form.Group>

            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>{" "}
        </>
      )}
      {loading && <Loading />}
    </div>
  );
}

export default Login;
