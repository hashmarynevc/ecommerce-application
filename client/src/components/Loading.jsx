import React, { useState, useEffect } from "react";
import CircularProgress from '@mui/material/CircularProgress';

function Loading(){
    return(
<CircularProgress className="spinner"/>
    );
}

export default Loading;