import React, { useState, useEffect } from "react";
import Header from "./Header";
import Home from "./buyer/Home";
import Cart from "./buyer/Cart";
import ProductView from "./buyer/ProductView";
import Signup from "./SignUp";
import axios from "axios";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Login from "./Login";
import Profile from "./buyer/Profile";
import Order from "./buyer/Order";
import ProfileView from "./buyer/ProfileView";
import Dashboard from "./admin/Dashboard";
import Users from "./admin/Users";
import Search from "./buyer/Search";
import Orders from "./admin/Orders"
function App() {
  return (
    <Router>
      <div>
        <Header />
        <Routes>
          <Route exact path="/"  element={<Home />} />
          <Route exact path="/view/:id"  element={<ProductView />} />
          <Route exact path="/signup"  element={<Signup />} />
          <Route exact path="/login"  element={<Login />} />
          <Route exact path="/cart"  element={<Cart />} />
          <Route exact path="/create-profile/:id"  element={<Profile />} />
          <Route exact path="/order"  element={<Order />} />
          <Route exact path="/profile"  element={<ProfileView />} />
          <Route exact path="/dashboard"  element={<Dashboard />} />
          <Route exact path="/users"  element={<Users />} />
          <Route exact path="/search/:name"  element={<Search />} />
          <Route exact path="/viewOrders"  element={<Orders />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App
