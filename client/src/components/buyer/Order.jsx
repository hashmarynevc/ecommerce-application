import React, { useState, useEffect } from "react";
import { Button, Form, Card } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import Loading from "../Loading";
import Profile from "./Profile";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

function Order() {
  let navigate = useNavigate();
  const [profile, setProfile] = useState([]);
  const [orderProducts, setOrderProducts] = useState([]);
  const [paymentMethod, setPaymentMethod] = useState();
  const [profileExists, setProfileExists] = useState(true);
  const [orderConfirmed, setOrderConfirmed] = useState(false);
  const [refNo, setRefNo] = useState();

  function handleSubmit(e) {
    // const { name, value } = e.target;

    setPaymentMethod(e.target.value);
  }
  console.log(localStorage.getItem("currentUser"));
  useEffect(() => {
    axios
      .get(
        "http://localhost:3000/api/profile/" +
          localStorage.getItem("currentUser")
      )
      .then((res) => setProfile(res.data))
      //.then((res) => console.log(res.data))
      .catch((err) => {
        if (err.response.status === 404) {
          console.log(err.response.data.error);
          setProfileExists(false);
        }
      });

  }, []);

  let itemsArr = JSON.parse(localStorage.getItem("cartItems"));
  let itemsObj = [];
  if(itemsArr!==null){
    itemsArr.forEach(function (item, index) {
      itemsObj.push({ product: item.id, quantity: item.qty });
      axios
      .get(
        "http://localhost:3000/api/products/" +
        item.id
      )
      .then((res) => orderProducts(orderProducts[index]=res.data))
      //.then((res) => console.log(res.data))
      .catch((err) => {
        if (err.response.status === 404) {
          console.log(err.response.data.error);
        }
      });
    });
  
  }
  
  async function placeOrder() {
    itemsArr.forEach( function(item, index) {
      let updatedQty = Number(orderProducts[index].quantity) - Number(item.qty);
      const editProduct ={
        quantity:updatedQty
      }
      axios
      .put("http://localhost:3000/api/products/edit/" + item.id, editProduct)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error.response);
      });
    });
    console.log(orderProducts);
    console.log(itemsObj)
    const order = {
      user: localStorage.getItem("currentUser"),
      products: itemsObj,
      address: profile.address,
      payment: paymentMethod,
      price: localStorage.getItem("totalPrice"),
      status:'Confirmed'
    };

    console.log(order);

    axios
      .post("http://localhost:3000/api/order/create", order)
      .then(function (response) {
        setOrderConfirmed(true);
        const emailDetails = {
          fname:profile.firstName,
          productDetails: orderProducts,
          products: itemsObj,
          address: profile.address,
          payment: paymentMethod,
          price: localStorage.getItem("totalPrice")
        };
        console.log(emailDetails.fname)
        axios
        .post("http://localhost:3000/api/order/send-mail/" + localStorage.getItem("currentUser"), emailDetails)
        .then(function (response) {
          console.log(response)
        })
        .catch(function (error) {
          console.log(error.response);
        });
        localStorage.setItem("cartItems", null);
        localStorage.setItem("totalPrice", 0);
        setRefNo(response.data.orderRef);
      })
      .catch(function (error) {
        console.log(error.response);
      });
      
  }
  return (
    <div>
      <div style={{ display: orderConfirmed ? "block" : "none" }}>
        <Card className="orderConfrimed" style={{ width: "50rem" }}>
          <CheckCircleIcon
            style={{
              color: "green",
              fontSize: "70px",
              textAlign: "center",
              position: "relative",
              top: "50%",
              left: "50%",
            }}
          />

          <p> Your order is confirmed</p>
          <p>Reference Number: {refNo}</p>
        </Card>
      </div>

      <div style={{ display: orderConfirmed ? "none" : "block" }}>
        <div style={{ display: profileExists ? "none" : "block" }}>
          <p style={{ textAlign: "center", padding: "2%", fontSize: "20px" }}>
            Please set up your profile before you checkout
          </p>

          <Profile />
        </div>
        <div style={{ display: profileExists ? "block" : "none" }}>
          <h1 className="heading">Confirm Order</h1>

          <Card style={{ width: "50rem" }} className="cartPage">
            <Card.Body>
              <p>
                <label className="label">Full Name: </label> {profile.firstName}{" "}
                {profile.lastName}
              </p>
              <p>
                <label className="label">Contact Number: </label>{" "}
                {profile.contactNumber}
              </p>
              <p>
                <label className="label">Shipping Address: </label>{" "}
                {profile.address}
              </p>
              <p> <label className="label">Your total is:  </label>{" "}${localStorage.getItem("totalPrice")}</p>
              <p>
                <label className="label">Payment Method</label>
                <Form onChange={handleSubmit}>
                  <Form.Select name="paymentMethod">
                    <option>Select a payment method</option>
                    <option value="COD">Cash on Delivery</option>
                    <option value="Debit">Debit Card</option>
                    <option value="Credit">Credit Card</option>
                  </Form.Select>
                </Form>
              </p>
              <div className="checkout mt-3">
                <Button
                  variant="outline-dark"
                  onClick={placeOrder}
                  type="submit"
                >
                  Place Order
                </Button>
              </div>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

export default Order;
