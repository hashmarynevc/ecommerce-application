import React, { useState, useEffect } from "react";
import { Button, Form, Card } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import Loading from "../Loading";
import Profile from "./Profile";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

function ProfileView() {
  let navigate = useNavigate();
  const [profile, setProfile] = useState([]);

  const [profileExists, setProfileExists] = useState(true);
  console.log(localStorage.getItem("currentUser"));
  useEffect(() => {
    axios
      .get(
        "http://localhost:3000/api/profile/" +
          localStorage.getItem("currentUser")
      )
      .then((res) => setProfile(res.data))
      //.then((res) => console.log(res.data))
      .catch((err) => {
        if (err.response.status === 404) {
          console.log(err.response.data.error);
          setProfileExists(false);
        }
      });
  }, []);
  return (
    <div>
      <div style={{ display: profileExists ? "none" : "block" }}>
        <Profile />
      </div>

      <div style={{ display: profileExists ? "block" : "none" }}>

     
        <h1 className="heading">Your Profile</h1>
        <div className="profileIcon">
        <AccountCircleIcon fontSize="100px" />
        </div>
        <Card style={{ width: "50rem" }} className="profile">
        
        <Card.Title style={{ fontSize: "40px" }}>
        <p>
             {profile.firstName}{" "}
              {profile.lastName}
            </p>
        </Card.Title>
          <Card.Body>
           
            <p>
              
              {profile.contactNumber}
            </p>
            <p>
             
              {profile.address}
            </p>
          </Card.Body>
        </Card>
      </div>
    </div>
  );
}

export default ProfileView;
