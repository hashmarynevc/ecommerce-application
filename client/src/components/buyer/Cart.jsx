import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";
import { Card, Col, Row, Button } from "react-bootstrap";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import updatePrice from "../../utils/updatePrice";
import CartProduct from "./CartProduct";

function Cart() {
  const [products, setProduct] = useState([]);
  let itemsArr = JSON.parse(localStorage.getItem("cartItems"));
  const [cartEmpty, setCartEmpty] = useState(itemsArr === null);
  let cartProducts = new Array();
  const [total, setTotal] = useState(localStorage.getItem("totalPrice"));
  //console.log(JSON.parse(localStorage.getItem("cartItems")));
  const [quantity, setQuantity] = useState({ clicks: 0 });

  useEffect(() => {
    console.log(localStorage.getItem("cartItems") === null);
    if (!cartEmpty) {
      itemsArr.forEach(function (item) {
        axios
          .get("http://localhost:3000/api/products/" + item.id)
          .then((res) =>
            setProduct((prevItems) => {
              return [...prevItems, res.data];
            })
          )
          //.then((res) => console.log(res.data))
          .catch((err) => console.error(err));
      });
    }
    setTotal(localStorage.getItem("totalPrice"));
  }, []);

  products.map((item, index) => {
    item.key = Math.random();
    item.inputQty = itemsArr[index].qty;
  });
  console.log(products);

  async function hanldeChange(newQTY, index) {
    // const form = e.target;
    // let id = Number(form.id.substring(1));
    // itemsArr[id].qty = form.value;
    // products[id].inputQty = form.value;
    // console.log(products[id].inputQty);
    // console.log(id);
     itemsArr[index].qty = newQTY;
    products[index].inputQty = newQTY;
    console.log(products[index].inputQty);
    let itemTotal = updatePrice(products, itemsArr);

    // products.forEach((product, index) => {
    //   itemTotal = itemTotal + updatePrice(Number(product.price),Number(itemsArr[index].qty))
    // });

    localStorage.setItem("totalPrice", itemTotal);
    setTotal(localStorage.getItem("totalPrice"));
  }

  function deleteItem(index) {
    // console.log(index)
    console.log(index);
    let deletePrice = itemsArr[index].qty * Number(products[index].price);
    localStorage.setItem(
      "totalPrice",
      Number(localStorage.getItem("totalPrice") - deletePrice)
    );

   
   itemsArr.splice(index, 1);
    localStorage.setItem("cartItems", JSON.stringify(itemsArr));

    //updateProducts();
  window.location.reload();
  }
  // async function updatePrice(){
  //   let itemTotal=0;

  //   products.forEach((product, index) => {
  //     itemTotal = itemTotal + (Number(product.price)*Number(itemsArr[index].qty));
  //   });

  //   localStorage.setItem("totalPrice", itemTotal);
  //   setTotal(localStorage.getItem("totalPrice"));
  //   console.log(itemTotal);
  // }

  return (
    <div>
      <h1 className="heading">Cart</h1>

      <Card style={{ width: "50rem" }} className="cartPage">
        <Card.Body>
          <Card.Text style={{ display: cartEmpty ? "block" : "none" }}>
            Your cart is empty
          </Card.Text>
          <div style={{ display: cartEmpty ? "none" : "block" }}>
            {products.map((product, index) => {
              return (
                <Card className="cart">
                  <Row>
                    <CartProduct
                      key={index}
                      index={index}
                      id={product._id}
                      name={product.name}
                      price={product.price}
                      quantity={product.inputQty}
                      image={product.imageURL}
                      onDelete={deleteItem}
                      onChange={hanldeChange}
                    />
                  </Row>
                  {/* <Row>
                  <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                    <img className="cartProdImg" src={product.imageURL} />
                  </Col>
                  <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                    <h2>{product.name}</h2>
                    <p></p>
                  </Col>
                  <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                    <p>${product.price}</p>
                  </Col>
                  <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                    <Row>
                      <Col>
                      <form>
                      <input hidden defaultValue={product.price}></input>
                      <input
                          defaultValue={product.inputQty}
                          type="number"
                          className="cartQty"
                          min="0"
                          id={"#"+index}
                          onChange={
                            hanldeChange
                            }
                        />
                      </form>
                       
                      </Col>
                      <Col>
                        <span>
                          <DeleteOutlineIcon  className="deleteIcon"
                            onClick={() => {
                              deleteItem(index);
                            }}
                          />
                        </span>
                      </Col>
                    </Row>
                  </Col>
                </Row> */}
                </Card>
              );
            })}
            <Row>
              <Col></Col>
              <Col>
                <p className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                  Total
                </p>
              </Col>
              <Col>
                <p className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
                  ${total}
                </p>
              </Col>
              <Col></Col>
            </Row>
            <div className="checkout mt-3">
              <Link to="/order">
                <Button variant="outline-dark">Proceed To Checkout</Button>
              </Link>
            </div>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Cart;
