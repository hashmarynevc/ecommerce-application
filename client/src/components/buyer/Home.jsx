import React, { useState, useEffect } from "react";
import Product from "./Product";
import axios from "axios";
import { Row, Col } from "react-bootstrap";
import { Link, Navigate, useNavigate, useParams } from "react-router-dom";
import Loading from "../Loading";

function Home() {
  const params = useParams();
  const [products, setProducts] = useState([]);
  const [searchProducts, setSearchProducts] = ([]);
 
  useEffect(() => {
    
      axios
      .get("http://localhost:3000/api/products")
      .then((res) => setProducts(res.data))
      //.then((res) => console.log(res.data[0]._id))
      .catch((err) => console.error(err));
    

    // if(params.name!==undefined){
    //   console.log(params.name);
    //   axios
    //   .get("http://localhost:3000/api/products/" + params.name)
    //   .then((res) => setSearchProducts(res.data))
    //   //.then((res) => console.log(res.data[0]._id))
    //   .catch((err) => console.error(err));
    // }

    // axios
    // .get("http://localhost:3000/api/products/:name")
    // .then((res) => setSearchProducts(res.data))
    // //.then((res) => console.log(res.data[0]._id))
    // .catch((err) => console.error(err));

      
  }, []);

  
  return (
 
    <div className="productPage container" >
    <Row>
    <Col>
    {products.map((product, index) => {
        return (
          <Product
            key={index}
            id={product._id}
            name={product.name}
            price={product.price}
            description={product.description}
            quantity={product.quantity}
            image={product.imageURL}
          />
         
         
        );
      })}
    </Col>

   
    
    
    </Row>
      

    </div>
 
  );
}

export default Home;
