import React, { useState, useEffect } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { Link, Navigate, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import Loading from "../Loading";
function Profile() {
  const params = useParams();
  let currentUser;
  let alreadyLoggedIn=false;
  if(localStorage.getItem('currentUser')){
    currentUser=localStorage.getItem('currentUser');
    alreadyLoggedIn=true;
  }
  else{
    currentUser=params.id;
    alreadyLoggedIn=false;
  }
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [profile, setProfile] = useState({
    user: currentUser,
    fname: "",
    lname: "",
    contact: "",
    address: "",
  });
  let navigate = useNavigate();
  function handleChange(event) {
    const { name, value } = event.target;
 
    setProfile({ ...profile, [name]: value });
  }

  async function handleSubmit(event) {
    event.preventDefault();
   
    console.log(profile);
    setLoading(true);
    axios
      .post("http://localhost:3000/api/profile/create", profile)
      .then(function (response) {
        console.log(response.data.message);
        if (response.data.message === "sucess") {
          setTimeout(() => {
            setLoading(false);
            if(alreadyLoggedIn){
              window.location.reload();
            }else{
              navigate("/login");

            }
            
          }, 2000);
        }
      })
      .catch(function (error) {
        console.log(error.response);
      });

      
  }



  return (
    <div>
      {/* {loading && <Loading />} */}
      <h1 className="heading">Setup your profile</h1>

      <Form className="formCard" border="dark" onSubmit={handleSubmit}>
        <Form.Text
          className="mb-3 errors"
          style={{ display: isError ? "block" : "none" }}
        >
          Invalid email or password. Please try again.
        </Form.Text>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            name="fname"
            type="text"
            placeholder="Enter first name"
            onChange={handleChange}
            required

          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Last Name</Form.Label>
          <Form.Control
            name="lname"
            type="text"
            placeholder="Enter last name"
            onChange={handleChange}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Contact Number</Form.Label>
          <Form.Control
            name="contact"
            type="tel"
            placeholder="Enter contact number"
            onChange={handleChange}
            required
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Address</Form.Label>
          <Form.Control
            name="address"
            type="text"
            placeholder="Enter address"
            onChange={handleChange}
            required
          />
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    </div>
  );
}

export default Profile;
