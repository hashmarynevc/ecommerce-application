import React, { useState, useEffect } from "react";
import TextTruncate from "react-text-truncate";
import { Link } from "react-router-dom";
import { Container, Card, Button, CardGroup, Col, Row } from "react-bootstrap";

function Product(props) {
  
  return (
 
<div className="products">
  <Card style={{ width: "18rem" }} className="productCard"> 
      <div className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
      <Card.Img variant="top" src={props.image} style={{width:"200px", height:"200px"}} />
      </div>
       
        <Card.Body>
          <Card.Title>
            {props.name}
           {props.quantity>=1 ? <span className="qty">{props.quantity} items left</span> :  <span className="qty">Out of stock</span>}
          </Card.Title>
          <Card.Text className="productText">
            <TextTruncate
              line={4}
              element="span"
              truncateText="…"
              text={props.description}
            />
          </Card.Text>
          <p className="productPrice">$ {props.price}</p>
          <Link to={`/view/${props.id}`} style={{ display: props.quantity>=1 ? "block" : "none" }}>
            <Button variant="outline-dark" className="addToCart">
              BUY
            </Button>
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
}

export default Product;
