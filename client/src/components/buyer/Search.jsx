import React, { useState, useEffect } from "react";
import Product from "./Product";
import axios from "axios";
import { Row, Col } from "react-bootstrap";
import { Link, Navigate, useNavigate, useParams } from "react-router-dom";
function Search() {
  const params = useParams();
  var product = [];
  const [searchProducts, setSearchProducts] = useState([]);
  var name = params.name;
  var url = "http://localhost:3000/api/products/search/" + name;
  console.log(url);
  useEffect(() => {
    axios
      .get(url)
      .then((res) => setSearchProducts(res.data))
      //.then((res) => console.log(res))
      .catch((err) => console.error(err));
  }, []);

  return (
    <div className="productPage container">
      <Row>
        <Col>
          {!searchProducts || (!searchProducts.length && <p>No such items</p>)}
          {searchProducts.map((product, index) => {
            return (
              <Product
                key={index}
                id={product._id}
                name={product.name}
                price={product.price}
                description={product.description}
                quantity={product.quantity}
                image={product.imageURL}
              />
            );
          })}
        </Col>
      </Row>
    </div>
  );
}

export default Search;
