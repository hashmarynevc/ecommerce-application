import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Card, Col, Row, Button } from "react-bootstrap";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

function ProductView(props) {
  
  const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

  const params = useParams();
  const [product, setProduct] = useState([]);
  const [open, setOpen] = useState(false);
  const [openNotLoggedIn, setOpenNotLoggedIn] = useState(false);

  const [isLoggedin, setLoggedIn] = useState(
    localStorage.getItem("currentUser") !== "undefined" &&
      localStorage.getItem("currentUser") !== null
  );

  useEffect(() => {
    axios
      .get("http://localhost:3000/api/products/" + params.id)
      .then((res) => setProduct(res.data))
      //.then((res) => console.log(res.data[0]._id))
      .catch((err) => console.error(err));
  }, []);
  const [quantity, setQuantity] = useState({ clicks: 1 });

  const IncrementItem = () => {
    if (quantity.clicks < product.quantity) {
      setQuantity({ clicks: quantity.clicks + 1 });
    }
  };
  const DecreaseItem = () => {
    if (quantity.clicks > 1) {
      setQuantity({ clicks: quantity.clicks - 1 });
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
    setOpenNotLoggedIn(false);
  };

  function addItemToCart() {
    if (
      localStorage.getItem("currentUser") !== "undefined" &&
      localStorage.getItem("currentUser") !== null
    ) {
     // let key = Math.random();
      let cartItems = [];
      if (localStorage.getItem("cartItems") !== null) {
        let localarr = JSON.parse(localStorage.getItem("cartItems"));
        //console.log(localarr);
        if(localarr!==null){
          for (let i = 0; i < localarr.length; i++) {
            cartItems.push(localarr[i]);
          }
        }
       
      }
      let item = { id: params.id, qty: quantity.clicks, price:product.price };
      cartItems.push(item);
      let total= Number(localStorage.getItem("totalPrice")) + (Number(item.price) * item.qty);
      localStorage.setItem("totalPrice", total);
      localStorage.setItem("cartItems", JSON.stringify(cartItems));
      setOpen(true);
    } else {
      setOpenNotLoggedIn(true);
    }
  }

  return (
    <div>
    
      <Card style={{ width: "18rem" }} className="singleProduct">
        <Row>
          <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
            <Card.Img
              variant="top"
              src={product.imageURL}
              className="prodImg"
              style={{width:"300px", height:"300px"}}
            />
          </Col>
          <Col className="position-relative">
            {" "}
            <Card.Body className="position-relative cardBody">
              <Card.Title>
                {" "}
                <h1 className="productTitle"> {product.name}</h1>
              </Card.Title>
              <Card.Text>{product.description}</Card.Text>

              <p className="productPrice">
                $ {product.price}{" "}
                <span className="single-qty">
                  {product.quantity} items left
                </span>
              </p>
            </Card.Body>
            <Button
              onClick={addItemToCart}
              variant="outline-dark"
              className="addToCart position-absolute fixed-bottom"
            >
              + Add To Cart
            </Button>
            <span className="qtyIncDecBtn">
              <RemoveIcon onClick={DecreaseItem} fontSize="large" />{" "}
              <input
                className="qtyInput"
                name="qty"
                value={quantity.clicks}
                type="number"
                min="0"
                max={product.quantity}
              />{" "}
              <AddIcon onClick={IncrementItem} fontSize="large" />
            </span>
          </Col>
        </Row>
        <Snackbar
          open={open}
          autoHideDuration={2000}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <Alert
            onClose={handleClose}
            severity="success"
            sx={{ width: "100%" }}
          >
            Item Added to Cart
          </Alert>
        </Snackbar>
        <Snackbar
          open={openNotLoggedIn}
          autoHideDuration={5000}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
            Please login to add products to cart
          </Alert>
        </Snackbar>
      </Card>
    </div>
  );
}

export default ProductView;
