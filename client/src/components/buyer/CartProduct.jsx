import { useParams } from "react-router-dom";
import axios from "axios";
import { Card, Col, Row, Button } from "react-bootstrap";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

function CartProduct(props) {
  return (
    <Row>
      <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
        <img className="cartProdImg" src={props.image} />
      </Col>
      <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
        <h2>{props.name}</h2>
        <p></p>
      </Col>
      <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
        <p>${props.price}</p>
      </Col>
      <Col className="d-flex flex-column min-vh-10 justify-content-center align-items-center">
        <Row>
          <Col>
            <form>
              <input hidden defaultValue={props.price}></input>
              <input
                defaultValue={props.quantity}
                type="number"
                className="cartQty"
                min="0"
                id={"#" + props.key}
                onChange={(e) => {
                  props.onChange(e.target.value, props.index);
                }}
              />
            </form>
          </Col>
          <Col>
            <span>
              <DeleteOutlineIcon
                className="deleteIcon"
                onClick={() => {
                  props.onDelete(props.index);
                }}
              />
            </span>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default CartProduct;
