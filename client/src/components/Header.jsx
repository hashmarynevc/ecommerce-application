import React, { useState } from "react";
import {Button, Form, Row, Col} from "react-bootstrap";
import { Link, Navigate , useNavigate } from "react-router-dom";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import LogoutIcon from "@mui/icons-material/Logout";
import LoyaltyIcon from "@mui/icons-material/Loyalty";
import PersonIcon from '@mui/icons-material/Person';
import Loading from "./Loading";

function Header() {
  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  console.log(localStorage.getItem("currentUser"));
  const [isLoggedin, setLoggedIn] = useState(
    localStorage.getItem("currentUser") !== "undefined" &&
      localStorage.getItem("currentUser") !== null
  );
  const [isAdmin, setIsAdmin]=useState(  localStorage.getItem("currentUser") !== "undefined" &&
  localStorage.getItem("currentUser") !== null && localStorage.getItem("userRole")==="admin");

  const [isSuperAdmin, setIsSuperAdmin]=useState(  localStorage.getItem("currentUser") !== "undefined" &&
  localStorage.getItem("currentUser") !== null && localStorage.getItem("userRole")==="superAdmin");
  function logout() {
    setLoading(true);
    localStorage.clear();
    setLoggedIn((prevValue) => {
      return !prevValue;
    });
    setLoading(false);
    navigate("/");
  }

  function handleSubmit(e){
    e.preventDefault();
    navigate("/search/" + e.target.search.value.charAt(0).toUpperCase() + e.target.search.value.slice(1) );
    window.location.reload();
  }

  return (
    <header>
      <h1>
      <Row>
        <Col>
        <Link to="/" className="logo">
          <LoyaltyIcon fontSize="large" /> QuickShop.com
        </Link>
        </Col>

        <Col>
        <Form className="d-flex" onSubmit={handleSubmit}>
        <Form.Control
          type="search"
          placeholder="Search"
          aria-label="Search"
          name="search"
        />
        <Button variant="outline-primary" type="submit">Search</Button>
      </Form>
        </Col>

        <Col>
        <Link to="/login">
          <Button
            className="login"
            variant="outline-light"
            style={{ display: isLoggedin ? "none" : "block" }}
          >
            Login
          </Button>
        </Link>
        <span>
          <LogoutIcon
            fontSize="large"
            className="headerIcons"
            style={{ display: isLoggedin ? "block" : "none" }}
           
            onClick={logout}
          />
          <Link to="/profile">
          <PersonIcon
            fontSize="large"
            className="headerIcons"
            style={{ display: isLoggedin  && !isAdmin && !isSuperAdmin ? "block" : "none" }}
           
          />
          </Link>

          <Link to="/cart">
          <ShoppingCartIcon
            fontSize="large"
            className="headerIcons"
            style={{ display: isLoggedin  && !isAdmin && !isSuperAdmin ? "block" : "none" }}
            
          />
          </Link>

         <Link to="/users">
          <Button
            className="login"
            variant="outline-light"
            style={{ display: isLoggedin && isSuperAdmin ? "block" : "none" }}
          >
            + New Admin
          </Button>
        </Link>

        <Link to="/viewOrders">
          <Button
            className="login"
            variant="outline-light"
            style={{ display: isLoggedin && (isSuperAdmin || isAdmin) ? "block" : "none" }}
          >
            View Orders
          </Button>
        </Link>
          
        </span>
        </Col>
      </Row>
        
       

        
      </h1>
      
    </header>
   
  );
}

export default Header;
