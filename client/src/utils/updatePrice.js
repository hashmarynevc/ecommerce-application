const updatePrice = (products, items) => {
  let itemTotal = 0;

  if (items === undefined || items.length === 0 || products === undefined || products.length === 0) {
    itemTotal = 0;
  } else {
    products.forEach((product, index) => {
      itemTotal = itemTotal + Number(product.price) * Number(items[index].qty);
    });
  }
  return itemTotal;
};

module.exports = updatePrice;
