const updatePrice = require("../src/utils/updatePrice");

test("properly calculates total price of items in cart", () => {
  let productsArr = [{ price: 200 }, { price: 400 }, { price: 100 }];
  let itemsArr = [{ qty: 2 }, { qty: 1 }, { qty: 3 }];
  expect(updatePrice(productsArr, itemsArr)).toBe(1100);
});

test("return 0 if items array is emptu", () => {
  let productsArr = [{ price: 200 }, { price: 400 }, { price: 100 }];
  let itemsArr = [];
  expect(updatePrice(productsArr, itemsArr)).toBe(0);
});
