const fs = require('fs');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

// Load env vars
dotenv.config({ path: './test.env' });

// Load models
const Product = require('./models/Products.js');
const Order = require('./models/Order');
const User = require('./models/Users.js');
const Profile = require('./models/Profile.js');

mongoose.connect(process.env.MONGODB_URL, {
	useNewUrlParser: true,
	useCreateIndex: true,
	useFindAndModify: false,
	useUnifiedTopology: true,
});

// Read JSON files
const products = JSON.parse(
	fs.readFileSync(`${__dirname}/__data__/products.json`, 'utf-8')
);
const users = JSON.parse(
	fs.readFileSync(`${__dirname}/__data__/users.json`, 'utf-8')
);
const profile = JSON.parse(
	fs.readFileSync(`${__dirname}/__data__/profile.json`, 'utf-8')
);
const orders = JSON.parse(
	fs.readFileSync(`${__dirname}/__data__/orders.json`, 'utf-8')
);


const importData = async () => {
	try {
		const promises = [Product.create(products), User.create(users), Order.create(orders), Profile.create(profile)];
		await Promise.all(promises);

		
	} catch (err) {
		console.error(err);
	}
};

// Delete data
const deleteData = async () => {
	try {
		const promises = [Product.deleteMany(), User.deleteMany(), Order.deleteMany(),Profile.deleteMany() ];
		await Promise.all(promises);
	} catch (err) {
		console.error(err);
	}
};

exports.setupDatabase = async () => {
	await deleteData();
	await importData();
};

