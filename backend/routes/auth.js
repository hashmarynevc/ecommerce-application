const express = require("express");

//const express = require('express');

const { signUp, login, editUser, getUsers } = require("../controllers/auth");

const router = express.Router();

router.route("/signup").post(signUp);
router.route("/login").post(login);
router.route("/user/edit/:id").put(editUser);
router.route("/users").get(getUsers);
module.exports = router;
