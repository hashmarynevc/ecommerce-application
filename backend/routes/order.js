const express = require("express");
const { send } = require("express/lib/response");

const { createOrder, sendMail, getOrders, editOrder } = require("../controllers/order");

const router = express.Router();

router.route("/order/create").post(createOrder);
router.route("/order/send-mail/:id").post(sendMail);
router.route("/orders").get(getOrders);
router.route("/order/edit/:id").put(editOrder);
module.exports = router;
