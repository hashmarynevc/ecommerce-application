const express = require("express");

const { getProfile, createProfile, editProfile } = require("../controllers/profile");

const router = express.Router();

router.route("/profile/create").post(createProfile)
router.route("/profile/:id").get(getProfile);
router.route("/profile/edit/:id").put(editProfile);

module.exports = router;
