const express = require("express");

//const express = require('express');

const { getProducts, getSingleProduct, createProduct, editProduct, deleteProduct, getProductsByName } = require("../controllers/products");

const router = express.Router();

router.route("/products").get(getProducts);
router.route("/products/:id").get(getSingleProduct);
router.route("/products/search/:name").get(getProductsByName);
router.route("/products/add").post(createProduct);
router.route("/products/edit/:id").put(editProduct);
router.route("/products/delete/:id").delete(deleteProduct);
module.exports = router;
