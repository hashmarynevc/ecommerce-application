const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user:{
        type:mongoose.Schema.ObjectId,
        ref:"User"
    },
    firstName:{
        type: String,
        required: true
    },
    lastName:{
        type: String,
        required: true
    },
    contactNumber:{
        type: String,
        required: true
    },
    address:{
        type: String,
        required: true
    },
   
},{timestamps:true});

module.exports = mongoose.model('Profile', ProfileSchema);
