const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
    },
    products: [
      {
        product: {
          type: mongoose.Schema.ObjectId,
          ref: "Product",
        },
        quantity: {
          type: Number,
          required: true,
        },
      },
    ],

    shippingAddress: {
      type: String,
      required: true,
    },
    paymentMethod: {
      type: String,
      required: true,
    },
    totalPrice:{
      type:Number,
      required: true
    },
    orderStatus: {
      type: String,
      required: true,
      enum: ['Confirmed', 'Dispatched', 'Delivered']
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Order", OrderSchema);
