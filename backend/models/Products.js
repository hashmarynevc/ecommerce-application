const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    description:{
        type: String
    },
    quantity:{
        type: Number,
        required: true
    },
    imageURL:{
        type:String,
        required: true
    }
})

module.exports = mongoose.model('Product', ProductSchema);
