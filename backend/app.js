require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const dbConnection = require('./config/db');
const app = express();
dbConnection();
const products = require('./routes/products');
const auth= require('./routes/auth.js');
const profile = require('./routes/profile.js');
const order = require('./routes/order.js');
const cors = require("cors");

const urlencodedParser = bodyParser.urlencoded({extended: false});
app.use(bodyParser.json(), urlencodedParser);
app.get("/", function(req, res){
    res.send("Server is up and running")
});
app.use(cors());
app.use('/api/', products);
app.use('/api/', auth);
app.use('/api/', profile);
app.use('/api/',  order);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

module.exports = app;