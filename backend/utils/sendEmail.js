const nodemailer = require("nodemailer");

function sendEmail(products,email, totalPrice, address){
    const transport = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      });

     let content  = products.reduce(function (a, b) {
        return (
          a +
          "<tr>" +
          "<td>" +
          b.name +
          "</td>"+
          "<td>"+
          " $" +
          b.price +
          "</td>"+
          "<td>"+
          b.qty +
          "</td>"+
          "</tr>"
        );
      }, "");

      let total = "<p>Total: $" +  totalPrice + "</p>";
      let shippingAddress = "<p>Shipping address: " + address + "</p";

      transport.sendMail({
        from: process.env.MAIL_FROM,
        to: email,
        subject: "Order Confirmation",
        html:
          `<div className="email" style="
            border: 1px solid black;
            padding: 20px;
            font-family: sans-serif;
            line-height: 2;
            font-size: 20px; 
            ">
            <h2>Order Confirmed</h2>
            
            <p> Hi, </p>` + 
            `<p>Your QuickShop order has been confirmed. Below are the items you ordered,</p>` + `<table><tr> <th>Name</th> <th>Price</th> <th>Quantity</th> </tr>` +
          content + `</table>`+ total + shippingAddress +
          `</div>`,
      });
}

module.exports = sendEmail;
