const request = require("supertest");
const app = require("../app");
jest.setTimeout(300000);

describe("GET /orders", () => {
  test("Should return all orders", async () => {
    const response = await request(app).get("/api/orders");
    expect(response.statusCode).toEqual(200);
  });
});

describe("PUT /orders/edit/:id", () => {
  test("should successfully update an order status", async () => {
    updateOrderID = "62297faaeb1b0e9f11c7e27d";
    const updateOrder = {
      user: "621dad3c76fa5802e5e8afe3",
      products: [
        {
          product: "621eea8617bd4208b8efc1ff",
          quantity: 1,
         
        },
        {
            product: "621f02d517bd4208b8efc299",
          quantity: 1,
        },
      ],
      shippingAddress: "295 Galle Road, 03, Colombo",
      paymentMethod: "COD",
      totalPrice: 310,
      orderStatus: "Dispatched",
    };
    const response = await request(app)
      .put("/api/order/edit/" + updateOrderID)
      .send(updateOrder)
      .expect(201);
    expect(response.body.message).toBe("sucess");
  });

  test("should return an a 400 status code if product id does not exist", async () => {
    const updateOrder = {
      user: "621dad3c76fa5802e5e8afe3",
      products: [
        {
          product: "621eea8617bd4208b8efc1ff",
          quantity: 1,
         
        },
        {
            product: "621f02d517bd4208b8efc299",
          quantity: 1,
        },
      ],
      shippingAddress: "295 Galle Road, 03, Colombo",
      paymentMethod: "COD",
      totalPrice: 310,
      orderStatus: "Dispatched",
    };
    incorrectID = "62297faaeb1b0e9f11c7e27";
    const response = await request(app)
      .put("/api/order/edit/" + incorrectID)
      .send(updateOrder);
    expect(response.statusCode).toEqual(400);
  });
});
