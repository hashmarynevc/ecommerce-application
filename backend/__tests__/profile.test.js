const request = require("supertest");
const app = require("../app");
jest.setTimeout(300000);

describe("GET /profile/:id", () => {
  test("Should return correct profile corresponding to the ID passed ", async () => {
    const idInDb = "621f003817bd4208b8efc26e";
    const profileInDb = {
      _id: "621f005c17bd4208b8efc274",
      user: "621f003817bd4208b8efc26e",
      firstName: "Testing",
      lastName: "Profile",
      contactNumber: "07794345600",
      address: "1st lane, colombo",
    };
    const response = await request(app).get("/api/profile/" + idInDb);
    expect(response.body).toMatchObject(profileInDb);
  });

  test("Should return a 404 status code if passed ID does not exist ", async () => {
    const incorrectID = "621f005c17bd4208b8efc";
    const response = await request(app).get("/api/profile/" + incorrectID);
    expect(response.statusCode).toEqual(400);
  });
});

describe("POST /profile/create", () => {
  test("Should sucessfully save a profile", async () => {
    const newProfile = {
      user: "62315210b583a7853e6a2be0",
      fname: "Testing",
      lname: "Profile",
      contact: "07794345600",
      address: "1st lane, colombo",
    };
    const response = await request(app)
      .post("/api/profile/create")
      .send(newProfile);
    expect(response.statusCode).toEqual(201);
  });
});


