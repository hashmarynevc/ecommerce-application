const request = require("supertest");
const app = require("../app");
jest.setTimeout(300000);
const { setupDatabase } = require("../seeder");

beforeAll(setupDatabase);
describe("GET /products", () => {
  test("Should return all products", async () => {
    const response = await request(app).get("/api/products");
    expect(response.statusCode).toEqual(200);
  });
});

describe("GET /products/:id", () => {
  test("Should return correct product corresponding to the ID passed ", async () => {
    const id = "621eea8617bd4208b8efc1ff";
    const product1 = {
      _id: "621eea8617bd4208b8efc1ff",
      name: "Sweater",
      price: 10,
      description:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore ",
      quantity: 9,
      imageURL:
        "http://res.cloudinary.com/gapstars/image/upload/v1646217076/raxrldyizyykla7f2byo.jpg",
    };
    const response = await request(app).get("/api/products/" + id);
    expect(response.body).toMatchObject(product1);
  });

  test("Should return a 400 status code if passed ID does not exist ", async () => {
    const id = "21eeb1317bd4208b8efc200";
    const response = await request(app).get("/api/products/" + id);
    expect(response.statusCode).toEqual(400);
  });
});

describe("GET search/products/:name", () => {
  test("Should return all products containing the name passed", async () => {
    const name = "Skirt";
    const foundItems = [
      {
        _id: "6221d58b66d79d168ea09145",
        name: "Skirt",
        price: 400,
        description: "lorem ipsum",
        quantity: 4,
        imageURL:
          "http://res.cloudinary.com/gapstars/image/upload/v1646384521/fxmnvzr3p3wfkmpgbj7h.jpg",
      },
      {
        _id: "6225dd6823c705d8623342a5",
        name: "Short Skirt",
        price: 200,
        description: "wewfh owefoi kjwelkfj fwei fiu wpoeuwpifeu efpiuwfi",
        quantity: 9,
        imageURL:
          "http://res.cloudinary.com/gapstars/image/upload/v1646648678/sdh2m2weullwbiv7qo9z.jpg",
      },
    ];
    const response = await request(app).get("/api/products/search/" + name);
    expect(response.body).toMatchObject(foundItems);
  });

  test("Should return an empty array if there arent any products containing the name passed", async () => {
    const name = "abc";
    const foundItems = [];
    const response = await request(app).get("/api/products/search/" + name);
    expect(response.body).toMatchObject(foundItems);
  });
});

describe("POST /products/add", () => {
  test("Should sucessfully save a product", async () => {
    const newProduct = {
      name: "Testing",
      price: 300,
      description: "testing product",
      quantity: 8,
      imageURL:
        "http://res.cloudinary.com/gapstars/image/upload/v1646648678/sdh2m2weullwbiv7qo9z.jpg",
    };
    const response = await request(app)
      .post("/api/products/add")
      .send(newProduct);
    expect(response.statusCode).toEqual(201);
  });
});

describe("PUT /products/edit/:id", () => {
  test("should successfully update product corresponding to the ID passed", async () => {
    const updatedProduct = {
      _id: "6221d58b66d79d168ea09145",
      name: "Skirt",
      price: 400,
      description: "lorem ipsum",
      quantity: 4,
      imageURL:
        "http://res.cloudinary.com/gapstars/image/upload/v1646384521/fxmnvzr3p3wfkmpgbj7h.jpg",
    };

    const id = "6221d58b66d79d168ea09145";
    const response = await request(app)
      .put("/api/products/edit/" + id)
      .send(updatedProduct)
      .expect(201);
    expect(response.body.message).toBe("sucess");
  });

  test("should return an a 400 status code if product id does not exist", async () => {
    const updatedProduct = {
      _id: "6221d58b66d79d168ea09145",
      name: "Skirt",
      price: 400,
      description: "lorem ipsum",
      quantity: 4,
      imageURL:
        "http://res.cloudinary.com/gapstars/image/upload/v1646384521/fxmnvzr3p3wfkmpgbj7h.jpg",
    };
    const id = "21eeb1317bd4208b8efc200";

    const response = await request(app)
      .put("/api/products/edit/" + id)
      .send(updatedProduct);
    expect(response.statusCode).toEqual(400);
  });
});
