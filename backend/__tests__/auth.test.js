const request = require("supertest");
const app = require("../app");
jest.setTimeout(300000);

describe("POST /signup", () => {
    test("Should sucessfully save an user", async () => {
      const newUser = {
        email: "testUser@gmail.com",
        password: "123",
        role:"user"
      };
      const response = await request(app)
        .post("/api/signup")
        .send(newUser);
      expect(response.statusCode).toEqual(201);
    });
  });

  describe("POST /login", () => {
    test("Should sucessfully login with correct credentials", async () => {
      const correctCredentials = {
        email: "testUser@gmail.com",
        password: "123"
      }
      const response = await request(app)
        .post("/api/login")
        .send(correctCredentials);
      expect(response.body.message).toEqual("Sucess");
      
    });

    test("Should return an error message if credentials are incorrect", async () => {
        const incorrectCredentials = {
          email: "testUser@gmail.com",
          password: "abc"
        };
        const response = await request(app)
          .post("/api/login")
          .send(incorrectCredentials);
        expect(response.body.message).toEqual("Invalid Username or Password");
      });
  });