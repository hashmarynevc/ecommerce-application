const Order = require("../models/Order");
const User = require("../models/Users");
const Product = require("../models/Products");
const nodemailer = require("nodemailer");
const sendEmail = require("../utils/sendEmail");

exports.createOrder = async (req, res) => {
  try {
    const newOrder = new Order({
      user: req.body.user,
      products: req.body.products,
      shippingAddress: req.body.address,
      paymentMethod: req.body.payment,
      totalPrice: req.body.price,
      orderStatus: req.body.status,
    });
    let order = await newOrder.save();

    res.status(201).json({ message: "sucess", orderRef: order._id });
  } catch (err) {
    res.status(400).json({ message: err });
  }
};


exports.getOrders = async (req, res) => {
  try {
    let docs = await Order.find({}, { __v: 0 }, { sort: { createdAt: -1 } });
    res.json(docs);
  } catch {
    res.status(400).json({ error: err });
  }
};

exports.editOrder = async (req, res) => {

  try {
    var order = await Order.findByIdAndUpdate(req.params.id, {
      $set: req.body,
    });
    res.status(201).json({ message: "sucess" });
  } catch (error) {
    res.status(400).json({ message: error });
  }

};

exports.sendMail = async (req, res) => {
  const userid = req.params.user;
  try{
    let user = await  User.findOne({ user: userid });
    email=user.email;
    const orderDetails = {
      firstname: req.body.fname,
      productDetails: req.body.productDetails,
      products: req.body.products,
      shippingAddress: req.body.address,
      paymentMethod: req.body.payment,
      totalPrice: req.body.price,
    };
    let firstName = orderDetails.firstname;
    console.log(orderDetails);

    let list = [];
    orderDetails.productDetails.forEach((item) => {
      list.push(item);
    });

    orderDetails.products.forEach((item, index) => {
      list[index].qty = item.quantity;
    });

    sendEmail(
      list,
      email,
      orderDetails.totalPrice,
      orderDetails.shippingAddress
    );

    res.status(201).json({ message: "sucess" });
  }catch(error){
    res.status(400).json({ message: error });
  }
  
};
