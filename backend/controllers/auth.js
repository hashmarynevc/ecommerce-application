const User = require("../models/Users");
const saltRounds = 10;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

exports.signUp = async (req, res) => {
  const password = req.body.password;
  const hashedPassword = await bcrypt.hash(password, saltRounds);
  console.log(hashedPassword);
  try {
    const newUser = new User({
      email: req.body.email,
      password: hashedPassword,
      role: req.body.role,
    });
    let saveUser = await newUser.save();
    console.log(saveUser);
    res.status(201).json({ message: "sucess", id: saveUser._id });
  } catch (err) {
    res.status(400).json({ message: err });
  }
};

exports.login = async (req, res, next) => {
  const userLogginIn = req.body;
  try {
    const foundUser = await User.findOne({ email: userLogginIn.email }).exec();
    console.log(foundUser);

    if (foundUser===null) {
      res.json({ message: "User not found" });
    }
    else{
      console.log(foundUser);
      bcrypt
        .compare(userLogginIn.password, foundUser.password)
        .then((isCorrect) => {
          if (isCorrect) {
            res.status(201).json({
              message: "Sucess",
              user: foundUser._id,
              role: foundUser.role,
            });
          } else {
            res.status(400).json({ message: "Invalid Username or Password" });
          }
        });
    } 
  } catch (error) {
    return res.status(500).json({ message: "Internal server error." });
  }
};

exports.editUser = async (req, res) => {
  try {
    var user = await User.findByIdAndUpdate(req.params.id, {
      $set: req.body,
    });
    res.status(201).json({ message: "sucess" });
  } catch (error) {
    res.status(400).json({ message: error });
  }
  // User.findByIdAndUpdate(
  //   req.params.id,
  //   {
  //     $set: req.body,
  //   },
  //   (error, data) => {
  //     if (error) {
  //       res.status(400).json({ message: error });
  //     } else {
  //       res.status(201).json({ message: "sucess" });
  //     }
  //   }
  // );
};

exports.getUsers = async (req, res) => {
  try{
    let users = await User.find({}, { __v: 0 });
    res.json(users);
  }
  catch{
    res.status(400).json({ error: err });
  }
  // User.find({}, { __v: 0 }, (err, docs) => {
  //   if (!err) {
  //     res.json(docs);
  //   } else {
  //     res.status(400).json({ error: err });
  //   }
  // });
};
