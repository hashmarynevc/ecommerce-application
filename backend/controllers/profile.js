const Profile = require("../models/Profile");

exports.getProfile = async (req, res) => {
  const userid = req.params.id;
  Profile.findOne({ user: userid }, function (err, foundList) {
    if (!err) {
      if(foundList){
        res.json(foundList);
      }else{
        res.status(404).json({ error: "Profile not found" });
      }
      
    } else {
      res.status(400).json({ error: err });
    }
  });
};

exports.createProfile = async (req, res) => {
  try {
    const newProfile = new Profile({
      user: req.body.user,
      firstName: req.body.fname,
      lastName: req.body.lname,
      contactNumber: req.body.contact,
      address: req.body.address,
    });
    let profile = await newProfile.save();
    res.status(201).json({ message: "sucess" });
  } catch (err) {
    res.status(400).json({ message: err });
  }
  // const newProfile = new Profile({
  //   user: req.body.user,
  //   firstName: req.body.fname,
  //   lastName: req.body.lname,
  //   contactNumber: req.body.contact,
  //   address: req.body.address,
  // });
  // console.log(newProfile);
  // newProfile.save(function (err) {
  //   if (err) {
  //     res.status(400).json({ message: err });
  //   } else {
  //     res.status(201).json({ message: "sucess" });
  //   }
  // });
  
};

exports.editProfile = async (req, res) => {
  try {
    var profile = await Product.findByIdAndUpdate(req.params.id, {
      $set: req.body,
    });
    res.status(201).json({ message: "sucess" });
  } catch (error) {
    res.status(400).json({ message: error });
  }
  // Profile.findByIdAndUpdate(
  //   req.params.id,
  //   {
  //     $set: req.body,
  //   },
  //   (error, data) => {
  //     if (error) {
  //       res.status(400).json({ message: error });
  //     } else {
  //       res.status(201).json({ message: "sucess" });
  //     }
  //   }
  // );
};
