const Product = require("../models/Products");

exports.getProducts = async (req, res) => {
  try {
    let products = await Product.find({}, { __v: 0 });
    res.json(products);
  } catch (err){
    res.status(400).json({ error: err });
  }
  // Product.find({}, { __v: 0 }, (err, docs) => {
  //   if (!err) {
  //     res.json(docs);
  //   } else {
  //     res.status(400).json({ error: err });
  //   }
  // });
};

exports.getSingleProduct = async(req,res) => {
  const productid = req.params.id;
  try {
    let products = await Product.findOne({_id: productid});
    res.json(products);
  } catch(err) {
    res.status(400).json({ error: err });
  }
  // Product.findOne({_id: productid}, function(err, foundList){
  //   if (!err) {
  //     res.json(foundList);
  //   } else {
  //     res.status(400).json({ error: err });
  //   }
    
  // });
};

exports.getProductsByName= async (req,res)=>{
  const productName = req.params.name;
  try {
    let products = await Product.find({name: { "$regex": productName, "$options": "i" }});
    res.json(products);
  } catch (err){
    res.status(400).json({ error: err });
  }

  // Product.find({name: { "$regex": productName, "$options": "i" }}, function(err, foundList){
  //   if (!err) {
  //     res.json(foundList);
  //     console.log(foundList);
  //   } else {
  //     res.status(400).json({ error: err });
  //   }
    
  // });
};

exports.createProduct = async (req, res) => {

  try {
    const newProduct = new Product({
      name: req.body.name,
      price: req.body.price,
      description: req.body.description,
      quantity: req.body.quantity,
      imageURL: req.body.imageURL,
    });
    let product = await newProduct.save();
    res.status(201).json({ message: "sucess" });
  } catch (err) {
    res.status(400).json({ message: err });
  }

  // const newProduct = new Product({
  //   name: req.body.name,
  //   price: req.body.price,
  //   description: req.body.description,
  //   quantity: req.body.quantity,
  //   imageURL: req.body.imageURL,
  // });
  // console.log(newProduct);
  
  // newProduct.save(function (err) {
  //   if (err) {
  //     res.status(400).json({ message: err });
  //   } else {
  //     res.status(201).json({ message: "sucess" });
  //   }
  // });
  
};

exports.editProduct = async (req, res) => {
  try {
    var product = await Product.findByIdAndUpdate(req.params.id, {
      $set: req.body,
    });
    res.status(201).json({ message: "sucess" });
  } catch (error) {
    res.status(400).json({ message: error });
  }
  
  // Product.findByIdAndUpdate(
  //   req.params.id,
  //   {
  //     $set: req.body,
  //   },
  //   (error, data) => {
  //     if (error) {
  //       res.status(400).json({ message: error });
  //     } else {
  //       res.status(201).json({ message: "sucess" });
  //     }
  //   }
  // );
};

exports.deleteProduct = async (req, res) => {

  try {
    let product = await Product.findOneAndDelete({ _id: req.params.id });
    res.status(201).json({ message: "sucess" });
  } catch(err) {
    res.status(400).json({ error: err });
  }
  // Product.findOneAndDelete({ _id: req.params.id }, function (err) {
  //   if (err) {
  //     res.status(400).json({ message: err });
  //   } else {
  //     res.status(201).json({ message: "sucess" });
  //   }
  // });
};

